import React,  {useEffect, useState} from 'react'
import {loadTweets} from '../lookup'

export function TweetsComponent(props) {
    const textAreaRef = React.createRef()

    const[newTweets, setNewTweets] = useState([])

    const handleSubmit = (event) => {
        event.preventDefault()
        const newVal = textAreaRef.current.value
        let tempNewTweets = [...newTweets]
        tempNewTweets.unshift({
            content: newVal,
            likes: 0,
            id: 12313
        })
        setNewTweets(tempNewTweets)

        textAreaRef.current.value = ''
    }
    return <div className={props.className}>
        <div className='col-12 mb-3'>
        <form onSubmit={handleSubmit}>
            <textarea ref={textAreaRef} required={true} className='form-control'>

            </textarea>
        <button type='submit' className='btn btn-primary my-3'>Tweet</button>
    </form>
    </div>
        <TweetList newTweets={newTweets} />
    </div>
}


export function ActionBtn(props){
    const {tweet, action} = props
    const [likes, setLikes] = useState(tweet.likes ? tweet.likes : 0)
    const [userLike, setUserLike] = useState(tweet.userLike === true ? true : false)
    const className = props.className ? props.className : 'btn btn-primary btn-sm'
    const actionDisplay = action.display ? action.display : 'Action'

    const handleClick = (event) => {
        event.preventDefault()
        if (action.type === 'like') {
            if(userLike === true){
                setLikes(likes-1)
                setUserLike(false)
            } else {
                setLikes(likes+1)
                setUserLike(true)
            }
        }
    }
    const display = action.type === 'like' ? `${likes} ${actionDisplay}` : actionDisplay
    return <button className={className} onClick={handleClick}>{display}</button>
}

export function TweetList(props) {
    console.log(props.newTweets)
    const [tweetsInit, setTweetsInit] = useState([])
    useEffect(() => {
        const myCallBack = (response, status) => {
            if (status === 200) {
                setTweetsInit(response)
            } else {
                alert('wheres my money')
            }
        }
        loadTweets(myCallBack)
    }, [])
    return tweetsInit.map((item, index)=>{
        return <Tweet tweet={item} className='my-5 py-5 bg-white text-dark border' />
    })
}

export function Tweet(props) {
    const {tweet} = props
    const className = props.className ? props.className : 'col-10 mx-auto col-md-6'
    return <div className={className}>
        <p>{tweet.id} - {tweet.content}</p>
            <div className='btn btn-group'>
                <ActionBtn tweet={tweet} action={{type:"like", display:"Like"}}/>
                <ActionBtn tweet={tweet} action={{type:"unlike", display:"UnLike"}}/>
                <ActionBtn tweet={tweet} action={{type:"retweet", display:"Retweet"}}/>
            </div>
        </div>
}
