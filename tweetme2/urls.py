
from django.contrib import admin
from django.urls import path, include
from tweets.views import (
    home_view,
    tweet_detail_view,
    tweet_delete_view,
    tweet_action_view,
    tweet_list_view,
    tweet_create_view,
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view),
    path('tweet-create', tweet_create_view, name='tweet-create'),
    path('tweets', tweet_list_view, name='tweet-list'),
    path('tweets/<int:tweet_id>', tweet_detail_view, name='tweet-detail'),
    # path('api/tweets/action', tweet_action_view),
    # path('api/tweets/<int:tweet_id>', tweet_delete_view),
    path('api/tweets/', include('tweets.urls')),
]
